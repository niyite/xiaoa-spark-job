package com.xiaoa.spark;

import java.io.Serializable;

public class CallLog implements Serializable {
	private static final long serialVersionUID = 1L;
	public String callsign;
	public Double contactlat;
	public Double contactlong;
	public Double mylat;
	public Double mylong;
}
