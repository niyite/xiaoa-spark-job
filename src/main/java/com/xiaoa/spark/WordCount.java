/**
 * Illustrates a wordcount in Java
 */
package com.xiaoa.spark;

import java.util.Arrays;
import java.util.Iterator;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

public class WordCount {
	
	public static void main(String[] args) throws Exception {
		if(null == args || args.length == 0) {
			args = new String[3];
			args[0] = "local[*]";
			args[1] = "/home/xiaoa/source/a.txt";
			args[2] = "/home/xiaoa/source2/";
		}
		String master = args[0];
		SparkConf conf = new SparkConf();
		conf.setAppName("word-count");
//		conf.setMaster(master);
		JavaSparkContext sc = new JavaSparkContext(conf);
//		JavaSparkContext sc = new JavaSparkContext(master, "wordcount", System.getenv("SPARK_HOME"), System.getenv("JARS"));
		JavaRDD<String> rdd = sc.textFile(args[1]);
		JavaPairRDD<String, Integer> counts = rdd.flatMap(new FlatMapFunction<String, String>() {
			private static final long serialVersionUID = -5092042774757289673L;

			public Iterator<String> call(String x) {
				return Arrays.asList(x.split(" ")).iterator();
			}
		}).mapToPair(new PairFunction<String, String, Integer>() {
			private static final long serialVersionUID = 5182638841584181037L;

			public Tuple2<String, Integer> call(String x) {
				return new Tuple2<String, Integer>(x, 1);
			}
		}).reduceByKey(new Function2<Integer, Integer, Integer>() {
			private static final long serialVersionUID = 7916963682623186480L;

			public Integer call(Integer x, Integer y) {
				return x + y;
			}
		});
		counts.saveAsTextFile(args[2]);
	}
}
